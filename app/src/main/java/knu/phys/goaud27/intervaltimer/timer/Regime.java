package knu.phys.goaud27.intervaltimer.timer;

/**
 * Created by goaud27 on 5/23/15.
 */
public enum Regime {
    WAIT, WORK, REST, END
}
