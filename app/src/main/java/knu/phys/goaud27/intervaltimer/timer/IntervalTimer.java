package knu.phys.goaud27.intervaltimer.timer;


import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import java.util.HashMap;
import knu.phys.goaud27.intervaltimer.MainActivityFragment;
import knu.phys.goaud27.intervaltimer.TimeFragment;

/**
 * Created by goaud27 on 5/21/15.
 */

public class IntervalTimer {

    private final int minWork;
    private final int minRest;
    private final int secWork;
    private final int secRest;
    private final int cycles;

    private int currMin;
    private int currSec;
    private int currCycle;
    Regime currRegime;

    private int WorkStepInSec;
    private int RestStepInSec;

    private Thread tickThread;
    static public boolean pause = false;

    Activity activity;
    TimeFragment timeFragment;

    static MediaPlayer mediaPlayer = new MediaPlayer();

    public IntervalTimer(HashMap<String,Integer> hm) {
        minWork = hm.get(MainActivityFragment.MINUTES_WORK);
        minRest = hm.get(MainActivityFragment.MINUTES_REST);
        secWork = hm.get(MainActivityFragment.SECONDS_WORK);
        secRest = hm.get(MainActivityFragment.SECONDS_REST);
        int tempCycles = hm.get(MainActivityFragment.CYCLES);
        if(tempCycles != 0)
            cycles = tempCycles;
        else
            cycles = 1;

        currMin = minWork;
        currSec = secWork;
        currCycle = cycles;
        currRegime = Regime.WORK;
    }

    public IntervalTimer(HashMap<String,Integer> hm, HashMap<String,Integer> hm2) {
        minWork = hm.get(MainActivityFragment.MINUTES_WORK);
        minRest = hm.get(MainActivityFragment.MINUTES_REST);
        secWork = hm.get(MainActivityFragment.SECONDS_WORK);
        secRest = hm.get(MainActivityFragment.SECONDS_REST);
        cycles = hm.get(MainActivityFragment.CYCLES);

        currMin = minWork;
        currSec = secWork;
        currCycle = cycles;
        currRegime = Regime.WORK;

        double minWorkStep;
        double minRestStep;
        double secWorkStep;
        double secRestStep;

        if(cycles != 1) {
            minWorkStep =
                    (hm2.get(MainActivityFragment.MINUTES_WORK) - hm.get(MainActivityFragment.MINUTES_WORK)) / (cycles - 1.0);
            minRestStep =
                    (hm2.get(MainActivityFragment.MINUTES_REST) - hm.get(MainActivityFragment.MINUTES_REST)) / (cycles - 1.0);
            secWorkStep =
                    (hm2.get(MainActivityFragment.SECONDS_WORK) - hm.get(MainActivityFragment.SECONDS_WORK)) / (cycles - 1.0);
            secRestStep =
                    (hm2.get(MainActivityFragment.SECONDS_REST) - hm.get(MainActivityFragment.SECONDS_REST)) / (cycles - 1.0);
        } else {
            minWorkStep = 0;
            minRestStep = 0;
            secWorkStep = 0;
            secRestStep = 0;
        }

        WorkStepInSec = (int)Math.round(minWorkStep * 60 + secWorkStep);
        RestStepInSec = (int)Math.round(minRestStep * 60 + secRestStep);
        /*throw new NullPointerException("WorkStepInSec: " + WorkStepInSec + ", RestStepInSec: " + RestStepInSec
            + ", minWorkStep: " + minWorkStep + ", minRestStep: " + minRestStep
            + ", secWorkStep: " + secWorkStep + ", secRestStep: " + secRestStep);
            */
    }

    public void start(final Activity activity, final TimeFragment timeFragment) {
        tick(timeFragment, activity);
    }

    public void start2(final Activity activity, final TimeFragment timeFragment) {
        tick2(timeFragment, activity);
    }

    public void init(final Activity activity, final TimeFragment timeFragment) {
        this.activity = activity;
        this.timeFragment = timeFragment;
        timeFragment.updateMinView(minWork, activity);
        timeFragment.updateSecView(secWork, activity);
        timeFragment.updateCycleView(cycles, activity);
        currRegime = Regime.WAIT;
        timeFragment.updateRegimeView(currRegime, activity);
        timeFragment.setRegimeBackground(currRegime, activity);
    }

    public Thread getTickThread() {
        return tickThread;
    }

    public void pause() {
        pause = true;
    }

    public void wakeUp() {
        pause = false;
    }

    private void tick(final TimeFragment timeFragment, final Activity activity) {
        tickThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        try {
                            while(pause)
                                Thread.sleep(250);
                        } catch (InterruptedException e) {
                            this.interrupt();
                        }
                        Thread.sleep(1000);
                        if(currRegime == Regime.WAIT) {
                            currRegime = Regime.WORK;
                            timeFragment.updateRegimeView(currRegime, activity);
                            timeFragment.setRegimeBackground(currRegime, activity);
                        }
                        if(currSec != -1) {
                            timeFragment.updateSecView(currSec, activity);
                            currSec--;
                        } else {
                            if(currMin != 0) {
                                currSec = 59;
                                currMin--;
                                timeFragment.updateSecView(currSec, activity);
                                timeFragment.updateMinView(currMin, activity); // more than 1000 ms
                            } else {
                                if(currRegime == Regime.REST) {
                                    if (currCycle != 1) {
                                        currCycle--;
                                        timeFragment.updateCycleView(currCycle, activity);
                                        currRegime = Regime.WORK;
                                        timeFragment.updateRegimeView(currRegime, activity);
                                        timeFragment.setRegimeBackground(currRegime, activity);
                                        currSec = secWork;
                                        currMin = minWork;
                                        // signal EndRest
                                        playEnd(Regime.REST);
                                    } else {
                                        // End all
                                        currCycle--;
                                        timeFragment.updateCycleView(currCycle, activity);
                                        currRegime = Regime.END;
                                        timeFragment.setRegimeBackground(currRegime, activity);
                                        timeFragment.updateRegimeView(currRegime, activity);
                                        playEnd(Regime.REST);
                                        interrupt();
                                    }
                                } else {
                                    currRegime = Regime.REST;
                                    timeFragment.updateRegimeView(currRegime, activity);
                                    timeFragment.setRegimeBackground(currRegime, activity);
                                    currSec = secRest;
                                    currMin = minRest;
                                    timeFragment.updateMinView(currMin, activity);
                                    // signal EndWork
                                    playEnd(Regime.WORK);
                                }
                            }
                        }
                    }
                }catch(InterruptedException e){
                    // Ahtung! Hitler kaput
                }
            }
        };
        tickThread.start();
    }

    private void tick2(final TimeFragment timeFragment, final Activity activity) {

        tickThread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        try {
                            while (pause)
                                Thread.sleep(250);
                        } catch (InterruptedException e) {
                            this.interrupt();
                        }
                        Thread.sleep(1000);
                        if (currRegime == Regime.WAIT) {
                            currRegime = Regime.WORK;
                            timeFragment.updateRegimeView(currRegime, activity);
                            timeFragment.setRegimeBackground(currRegime, activity);
                        }
                        if (currSec != -1) {
                            timeFragment.updateSecView(currSec, activity);
                            currSec--;
                        } else {
                            if (currMin != 0) {
                                currSec = 59;
                                currMin--;
                                timeFragment.updateSecView(currSec, activity);
                                timeFragment.updateMinView(currMin, activity); // 59's second is longer)
                            } else {
                                if (currRegime == Regime.REST) {
                                    if (currCycle != 1) {
                                        currCycle--;
                                        timeFragment.updateCycleView(currCycle, activity);
                                        currRegime = Regime.WORK;
                                        timeFragment.updateRegimeView(currRegime, activity);
                                        timeFragment.setRegimeBackground(currRegime, activity);
                                        currMin = minWork;
                                        currSec = secWork;
                                        int cycleMult = cycles - currCycle;
                                        int currSecTemp =
                                                currSec + currMin * 60 +
                                                        cycleMult * WorkStepInSec;
                                        currMin = currSecTemp / 60;
                                        currSec = currSecTemp % 60;
                                        timeFragment.updateMinView(currMin, activity);
                                        // signal EndRest
                                        playEnd(Regime.REST);
                                    } else {
                                        // End all
                                        currCycle--;
                                        timeFragment.updateCycleView(currCycle, activity);
                                        currRegime = Regime.END;
                                        timeFragment.setRegimeBackground(currRegime, activity);
                                        timeFragment.updateRegimeView(currRegime, activity);
                                        playEnd(Regime.REST);
                                        interrupt();
                                    }
                                } else {
                                    currRegime = Regime.REST;
                                    timeFragment.updateRegimeView(currRegime, activity);
                                    timeFragment.setRegimeBackground(currRegime, activity);
                                    currMin = minRest;
                                    currSec = secRest;
                                    int cycleMult = cycles - currCycle;
                                    int currSecTemp =
                                            currSec + currMin * 60 +
                                                    cycleMult * RestStepInSec;
                                    currMin = currSecTemp / 60;
                                    currSec = currSecTemp % 60;
                                    timeFragment.updateMinView(currMin, activity);
                                    // signal EndWork
                                    playEnd(Regime.WORK);
                                }
                            }
                        }
                    }
                } catch(InterruptedException e) {
                    // Ahtung! Hitler kaput
                }
            }
        };
        tickThread.start();
    }

    void playEnd(Regime regime) {
        if(regime == Regime.WORK) {
            try {
                if(mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = new MediaPlayer();
                }
                mediaPlayer.reset();

                AssetFileDescriptor descriptorWork = activity.getAssets().openFd("beep21.mp3");
                mediaPlayer.setDataSource(descriptorWork.getFileDescriptor(),
                        descriptorWork.getStartOffset(), descriptorWork.getLength());
                descriptorWork.close();

                mediaPlayer.prepare();
                mediaPlayer.setVolume(1f, 1f);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                if(mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = new MediaPlayer();
                }
                mediaPlayer.reset();

                AssetFileDescriptor descriptorRest = activity.getAssets().openFd("beep2.mp3");
                mediaPlayer.setDataSource(descriptorRest.getFileDescriptor(),
                        descriptorRest.getStartOffset(), descriptorRest.getLength());
                descriptorRest.close();

                mediaPlayer.prepare();
                mediaPlayer.setVolume(0.9f, 0.9f);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
