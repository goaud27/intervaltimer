package knu.phys.goaud27.intervaltimer;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.HashMap;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment2 extends Fragment {

    private static long count = 0;
    private long id = count++;

    EditText editSecondsWork;
    EditText editSecondsRest;
    EditText editMinutesWork;
    EditText editMinutesRest;
    EditText editCycles;

    public static final String SECONDS_WORK = "SecondsWork";
    public static final String SECONDS_REST = "SecondsRest";
    public static final String MINUTES_WORK = "MinutesWork";
    public static final String MINUTES_REST = "MinutesRest";
    public static final String CYCLES = "Cycles";
    public static final String ZERO_VALUE = "0";

    public MainActivityFragment2() {
    }
    MainActivityFragment2 thisFragment = this; // It makes me laughing

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.fragment2View = inflater.inflate(R.layout.fragment_lvl2, container, false);
        editSecondsWork = (EditText) MainActivity.fragment2View.findViewById(R.id.edit_seconds_work2);
        editSecondsRest = (EditText) MainActivity.fragment2View.findViewById(R.id.edit_seconds_rest2);
        editMinutesWork = (EditText) MainActivity.fragment2View.findViewById(R.id.edit_minutes_work2);
        editMinutesRest = (EditText) MainActivity.fragment2View.findViewById(R.id.edit_minutes_rest2);
        editCycles = (EditText) MainActivity.fragment2View.findViewById(R.id.edit_cycles2);
        editCycles.setEnabled(false);

        //setRetainInstance(true); // handle screen rotate
        if(MainActivity.inputFix)
            fixAllEditText2();

        MainActivity.button2remove = (ImageButton) MainActivity.fragment2View.findViewById(R.id.action_remove2);
        MainActivity.button2remove.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.level2Ready) {
                    MainActivity.level2Ready = false;
                    MainActivity.button1remove =
                            (ImageButton) MainActivity.fragment1View.findViewById(R.id.action_remove);
                    MainActivity.button1removeState = true;
                    MainActivity.button1remove.setEnabled(MainActivity.button1removeState);
                }
                FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
                //transaction.addToBackStack(null);
                transaction.remove(thisFragment).commit();
                MainActivity.mainActivityFragment2 = null;
                MainActivity.addState = true;
                MainActivity.item_add.setEnabled(MainActivity.addState);
            }
        });

        addAddTextChangeListener60(editSecondsWork);
        addAddTextChangeListener60(editSecondsRest);
        addAddTextChangeListener9(editMinutesWork);
        addAddTextChangeListener9(editMinutesRest);
        //addAddTextChangeListener10(editCycles);

        return MainActivity.fragment2View;
    }

    public void fixAllEditText2() {
        editMinutesWork.setEnabled(false);
        editMinutesRest.setEnabled(false);
        editSecondsWork.setEnabled(false);
        editSecondsRest.setEnabled(false);
        //editCycles.setEnabled(false);

        try {
            int secWork = Integer.parseInt(editSecondsWork.getText().toString());
            editSecondsWork.setText(String.format("%02d", secWork));
        } catch (NumberFormatException e) {
            editSecondsWork.setText("00"); // not working
        }

        try {
            int secRest = Integer.parseInt(editSecondsRest.getText().toString());
            editSecondsRest.setText(String.format("%02d", secRest));
        } catch (NumberFormatException e) {
            editSecondsRest.setText("00"); // not working - bug?
        }
    }

    public long id() {
        return id;
    }

    private String getValue(EditText et) {
        String value = et.getText().toString();
        if(value.equals(""))
            return MainActivityFragment2.ZERO_VALUE;
        else
            return value;
    }

    private void setValue(EditText et, String value) {
        et.setText(value);
    }

    public HashMap<String,Integer> getTimeMap() {
        HashMap<String,Integer> hm = new HashMap<>();
        Integer minWorkValue = Integer.parseInt(getValue(editMinutesWork));
        Integer minRestValue = Integer.parseInt(getValue(editMinutesRest));
        Integer secWorkValue = Integer.parseInt(getValue(editSecondsWork));
        Integer secRestValue = Integer.parseInt(getValue(editSecondsRest));
        Integer cyclesValue = Integer.parseInt(getValue(editCycles));
        if(minWorkValue != 0)
            hm.put(MainActivityFragment2.MINUTES_WORK, minWorkValue);
        else {
            setValue(editMinutesWork, String.valueOf(0));
            hm.put(MainActivityFragment2.MINUTES_WORK, 0);
        }
        if(minRestValue != 0)
            hm.put(MainActivityFragment2.MINUTES_REST, minRestValue);
        else {
            setValue(editMinutesRest, String.valueOf(0));
            hm.put(MainActivityFragment2.MINUTES_REST, 0);
        }
        if(secWorkValue != 0)
            hm.put(MainActivityFragment2.SECONDS_WORK, secWorkValue);
        else {
            setValue(editSecondsWork, String.valueOf(0));
            hm.put(MainActivityFragment2.SECONDS_WORK, 0);
        }
        if(secRestValue != 0)
            hm.put(MainActivityFragment2.SECONDS_REST, secRestValue);
        else {
            setValue(editSecondsRest, String.valueOf(0));
            hm.put(MainActivityFragment2.SECONDS_REST, 0);
        }
        /*if(cyclesValue != 0)
            hm.put(MainActivityFragment2.CYCLES, cyclesValue);
        else {
            setValue(editCycles, String.valueOf(1));
            hm.put(MainActivityFragment2.CYCLES, 1);
        }*/
        hm.put(MainActivityFragment2.CYCLES, 0);
        return hm;
    }

    void addAddTextChangeListener60(final EditText et) {
        et.addTextChangedListener(new TextWatcher() {
            boolean textModified = false;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strEnteredVal = et.getText().toString();
                if(!strEnteredVal.equals("") && !(strEnteredVal.length() < 2)) {
                    int num = Integer.parseInt(strEnteredVal);
                    if (!textModified) {
                        if (num > 60) {
                            et.setText("");
                        } else {
                            if(num < 10) {
                                textModified = true;
                                et.setText(String.format("%02d", num));
                                et.setEnabled(false);
                            } else {
                                textModified = true;
                                et.setText("" + num);
                                et.setEnabled(false);
                            }
                        }
                    }
                }
            }

        });
    }

    /*void addAddTextChangeListener10(final EditText et) {
        et.addTextChangedListener(new TextWatcher() {
            boolean textModified = false;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strEnteredVal = et.getText().toString();
                if(!strEnteredVal.equals("")) {
                    int num = Integer.parseInt(strEnteredVal);
                    if (!textModified) {
                        if(num != 0) {
                            textModified = true;
                            et.setText("" + num);
                            et.setEnabled(false);
                        } else {
                            num = 1;
                            et.setText("" + num);
                        }
                    }
                }
            }

        });
    }*/

    void addAddTextChangeListener9(final EditText et) {
        et.addTextChangedListener(new TextWatcher() {
            boolean textModified = false;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String strEnteredVal = et.getText().toString();
                if(!strEnteredVal.equals("")) {
                    int num = Integer.parseInt(strEnteredVal);
                    if (!textModified) {
                        textModified = true;
                        et.setText("" + num);
                        et.setEnabled(false);
                    }
                }
            }

        });
    }


}
