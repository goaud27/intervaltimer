package knu.phys.goaud27.intervaltimer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import java.util.HashMap;

import knu.phys.goaud27.intervaltimer.timer.IntervalTimer;


public class MainActivity extends Activity {

    private static TimeFragment timeFragment;
    static MainActivityFragment mainActivityFragment1;
    static MainActivityFragment2 mainActivityFragment2;
    static IntervalTimer intervalTimer;
    static ImageButton button1remove;
    static ImageButton button2remove;

    static View fragment1View;
    static View fragment2View;
    static Menu menu;                   // static for onResume method
    static MenuItem item_restart;
    static MenuItem item_accept;
    static MenuItem item_add;

    static boolean level1Ready = false;
    static boolean level2Ready = false;
    static boolean inputFix = false;
    static HashMap<String,Integer> hm, hm2;

    static boolean addState = true;
    static boolean goState = false;
    static boolean acceptState = false;
    static boolean restartState = false;
    static boolean pauseAble = false;

    static boolean button1removeState = true;
    static boolean button2removeState = true;

    static boolean timeViewCreated = false;

    static Thread tickThread;
    AlertDialog aboutDialog;

    static PowerManager powerManager;

    HashMap<String,Integer> testHM = new HashMap<>();
    HashMap<String,Integer> testHM2 = new HashMap<>();

    {
        testHM.put(MainActivityFragment.MINUTES_WORK, 0);
        testHM.put(MainActivityFragment.MINUTES_REST, 0);
        testHM.put(MainActivityFragment.SECONDS_WORK, 3);
        testHM.put(MainActivityFragment.SECONDS_REST, 4);
        testHM.put(MainActivityFragment.CYCLES, 3);

        testHM2.put(MainActivityFragment.MINUTES_WORK, 0);
        testHM2.put(MainActivityFragment.MINUTES_REST, 0);
        testHM2.put(MainActivityFragment.SECONDS_WORK, 1);
        testHM2.put(MainActivityFragment.SECONDS_REST, 2);
        testHM2.put(MainActivityFragment.CYCLES, 0);

    }

    static int j = 0;

    public void createTimeView() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransactionGo = fragmentManager.beginTransaction();
        timeFragment = new TimeFragment();
        fragmentTransactionGo.add(R.id.time_fragment_container, timeFragment);
        fragmentTransactionGo.commit();
        fragmentManager.executePendingTransactions();
        timeViewCreated = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void onStart() {
        super.onStart();
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu; // hm
        menu.findItem(R.id.action_go).setEnabled(goState);
        menu.findItem(R.id.action_accept).setEnabled(acceptState);
        menu.findItem(R.id.action_restart).setEnabled(restartState);
        item_restart = menu.findItem(R.id.action_restart);
        item_accept = menu.findItem(R.id.action_accept);
        item_add = menu.findItem(R.id.action_add);
        item_add.setEnabled(addState);
        item_accept.setEnabled(acceptState);
        item_restart.setEnabled(restartState);
        if(pauseAble)
            menu.findItem(R.id.action_go).setIcon(R.drawable.ic_action_pause);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.action_add:
                acceptState = true;
                menu.findItem(R.id.action_accept).setEnabled(acceptState);
                if(!level1Ready) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransactionAdd = fragmentManager.beginTransaction();
                    mainActivityFragment1 = new MainActivityFragment();
                    fragmentTransactionAdd.add(R.id.main_fragment_container, mainActivityFragment1);
                    fragmentTransactionAdd.commit();
                    fragmentManager.executePendingTransactions();
                    level1Ready = true;
                    break;
                } else {
                    if(!level2Ready) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransactionAdd = fragmentManager.beginTransaction();
                        mainActivityFragment2 = new MainActivityFragment2();
                        fragmentTransactionAdd.add(R.id.main_fragment_container, mainActivityFragment2);
                        fragmentTransactionAdd.commit();
                        fragmentManager.executePendingTransactions();
                        level2Ready = true;
                        addState = false;
                        item.setEnabled(addState);
                        try {
                            //button1remove = (ImageButton) mainActivityFragment1.getView().findViewById(R.id.action_remove);
                            //it's get initialized in MainActivityFragment through static reference
                            button1removeState = false;
                            button1remove.setEnabled(button1removeState);
                        } catch (NullPointerException e) {
                            throw new NullPointerException("No button1remove was found");
                        }
                        break;
                    }
                }
                break;
            case R.id.action_accept:
                inputFix = true;
                acceptState = false;
                item.setEnabled(acceptState);
                addState = false;
                menu.findItem(R.id.action_add).setEnabled(addState);
                goState = true;
                menu.findItem(R.id.action_go).setEnabled(goState);
                if(level2Ready) {
                    mainActivityFragment1.fixAllEditText();
                    mainActivityFragment2.fixAllEditText2();
                    try {
                        //button2remove = (ImageButton) mainActivityFragment2.getView().findViewById(R.id.action_remove2);
                        button2removeState = false;
                        button2remove.setEnabled(button2removeState);
                    } catch (NullPointerException e) {
                        throw new NullPointerException("No button2remove was found");
                    }
                    hm = mainActivityFragment1.getTimeMap();
                    hm2 = mainActivityFragment2.getTimeMap();
                    intervalTimer =
                            new IntervalTimer(hm, hm2);
                    //intervalTimer = new IntervalTimer(testHM, testHM2); // test input
                    if(!timeViewCreated)
                        createTimeView();
                    intervalTimer.init(this, timeFragment);
                    break;
                } else {
                    if(level1Ready) {
                        mainActivityFragment1.fixAllEditText();
                        try {
                            //button1remove = (ImageButton) mainActivityFragment1.getView().findViewById(R.id.action_remove);
                            button1removeState = false;
                            button1remove.setEnabled(button1removeState);
                        } catch (NullPointerException e) {
                            throw new NullPointerException("No button1remove was found");
                        }
                        hm = mainActivityFragment1.getTimeMap();
                        intervalTimer =
                                new IntervalTimer(hm);
                        //intervalTimer = new IntervalTimer(testHM);
                        if(!timeViewCreated)
                            createTimeView();
                        intervalTimer.init(this, timeFragment);
                        break;
                    }
                }
                break;
            case R.id.action_go:
                if(tickThread != null) {
                    if (IntervalTimer.pause && tickThread.getState() == Thread.State.TIMED_WAITING) {
                        intervalTimer.wakeUp();
                        pauseAble = true;
                        menu.findItem(R.id.action_go).setIcon(R.drawable.ic_action_pause);
                        break;
                        //throw new NullPointerException("tickThread was paused" + tickThread.getState());
                    }
                }
                restartState = true;
                menu.findItem(R.id.action_restart).setEnabled(restartState);
                if(!pauseAble) {
                    pauseAble = true;
                    menu.findItem(R.id.action_go).setIcon(R.drawable.ic_action_pause);
                    if (level2Ready) {
                        intervalTimer.wakeUp();
                        intervalTimer.start2(this, timeFragment);
                        tickThread = intervalTimer.getTickThread();
                        break;
                    }
                    if (level1Ready) {
                        intervalTimer.wakeUp();
                        intervalTimer.start(this, timeFragment);
                        tickThread = intervalTimer.getTickThread();
                        break;
                    }
                } else {
                    // do pause or unpause
                    if(!IntervalTimer.pause && tickThread.getState() == Thread.State.TIMED_WAITING) {
                        intervalTimer.pause();
                        pauseAble = false;
                        menu.findItem(R.id.action_go).setIcon(R.drawable.ic_action_go);
                        //throw new NullPointerException("tickThread was paused" + tickThread.getState());
                    }
                }
                break;

            case R.id.action_refresh:
                if(intervalTimer != null)
                    tickThread = intervalTimer.getTickThread();
                intervalTimer = null;
                level1Ready = false;
                level2Ready = false;
                inputFix = false;
                addState = true;
                acceptState = false;
                goState = false;
                restartState = false;
                button1removeState = true;
                button2removeState = true;
                timeViewCreated = false;
                pauseAble = false;
                if(tickThread != null)
                    tickThread.interrupt();
                tickThread = null;
                Intent i = new Intent(this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                finish();
                startActivity(i);
                break;
            case R.id.action_restart:
                if(tickThread != null)
                    tickThread.interrupt();
                tickThread = null;
                pauseAble = false;
                acceptState = true;
                menu.findItem(R.id.action_accept).setEnabled(acceptState);
                onOptionsItemSelected(menu.findItem(R.id.action_accept));
                onOptionsItemSelected(menu.findItem(R.id.action_go));
                break;
            case R.id.action_about:
                displayAbout();
                break;


            default:
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    public void onPause() {
        super.onPause();
        //if(!inputFix) // add about dialog var
          //  onBackPressed();
        if(aboutDialog != null)
            aboutDialog.dismiss();
        boolean screenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            screenOn = powerManager.isInteractive();
        } else {
            screenOn = powerManager.isScreenOn();
        }
        if(screenOn) {
            if(intervalTimer != null && pauseAble)
                onOptionsItemSelected(menu.findItem(R.id.action_go));
        }
    }

    public void onResume() {
        super.onResume();
        if(intervalTimer != null && !pauseAble && goState && tickThread != null)
            onOptionsItemSelected(menu.findItem(R.id.action_go));
        // onCreateOptionsMenu is called after OnResume
        // unpase on screen rotation?
    }


    @Override
    public void onBackPressed() {
        finish();
        // nullify all static variables;
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public void displayAbout() {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.about, null );
        aboutDialog = new AlertDialog.Builder(this)
                .setView(dialoglayout)
                //.setTitle("About")
                //.setMessage(R.string.about)
                /*.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // close?
                    }
                })*/.show();
    }

    //onDestroy !!! static not good
}
