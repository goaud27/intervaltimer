package knu.phys.goaud27.intervaltimer;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import knu.phys.goaud27.intervaltimer.timer.Regime;

/**
 * Created by goaud27 on 5/21/15.
 */
public class TimeFragment extends Fragment {

    TextView minView;
    TextView secView;
    TextView cycleView;
    EditText regimeView;
    LinearLayout regimeLayout;
    int regimeBColor = Color.TRANSPARENT;

    public void updateMinView(final int currMin, final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                minView.setText(String.valueOf(currMin));
            }
        });
    }

    public void updateSecView(final int currSec, final Activity activity) { //any sense to move
                                                                        // runOnUiThread here?
        activity.runOnUiThread(new Runnable() {
        @Override
        public void run() {
            secView.setText(String.format("%02d", currSec));
        }
    });
    }

    public void updateCycleView(final int currCycle, final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cycleView.setText(String.valueOf(currCycle));
            }
        });
    }

    public void updateRegimeView(final Regime currRegime, final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                regimeView.setText(String.valueOf(currRegime));
            }
        });
    }

    public void setRegimeBackground(final Regime regime, final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(regime == Regime.WORK)
                    regimeLayout.setBackgroundColor(getResources().getColor(R.color.light_red));
                if(regime == Regime.REST)
                    regimeLayout.setBackgroundColor(getResources().getColor(R.color.light_green));
                if(regime == Regime.WAIT || regime == Regime.END)
                    regimeLayout.setBackgroundColor(getResources().getColor(R.color.lightest_sand));
                regimeBColor = ((ColorDrawable) regimeLayout.getBackground()).getColor();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_time, container, false);
        cycleView = (TextView) view.findViewById(R.id.cycle_view);
        secView = (TextView) view.findViewById(R.id.seconds_view);
        minView = (TextView) view.findViewById(R.id.minutes_view);
        regimeView = (EditText) view.findViewById(R.id.regime_view);
        regimeLayout = (LinearLayout) view.findViewById(R.id.regime_layout);
        regimeLayout.setBackgroundColor(regimeBColor);

        setRetainInstance(true); // handle screen rotate

        return view;
    }
}
